#include "CarColor.h"


CarColor::CarColor(const Color& color)
{
	this->_color = color;
}
CarColor::CarColor() {
	this->_color = black;
}
void CarColor::print() {
	std::cout << this->_color << std::endl;
}
Color CarColor::getColor()const {
	return this->_color;
}