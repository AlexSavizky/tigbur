#pragma once
#include <iostream>
enum Color {
    black = 0,
    white = 1,
    brown = 2,
};
class CarColor {
    Color color;
public:
    CarColor(const Color& color);
    CarColor();
    void print();
};
